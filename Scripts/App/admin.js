﻿/// <reference path="../Plugins/jQuery/jquery-3.1.0.min.js" />
/// <reference path="../Plugins/Angular/angular.min.js" />
/// <reference path="utility.js" />

$(document).ready(function () {
    Admin.initEvents();
    $("input[data-toggle=toggle]").bootstrapToggle().parent().addClass("toggle-resolve");
    $('#overlay').fadeOut();
});

var adminModule = angular.module("adminModule", []);

adminModule.factory('Base64', function () {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});

adminModule.directive('appFilereader', function ($q) {
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function () { };

            element.bind('change', function (e) {
                var element = e.target;

                $q.all(slice.call(element.files, 0).map(readFile))
                    .then(function (values) {
                        if (element.multiple) {
                            ngModel.$setViewValue(values);
                            $("#contentOfImage").attr("src", values);
                        }
                        else {
                            ngModel.$setViewValue(values.length ? values[0] : null);
                            $("#contentOfImage").attr("src", values[0]);
                        }
                    });

                function readFile(file) {
                    var deferred = $q.defer();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        deferred.resolve(e.target.result);
                    };
                    reader.onerror = function (e) {
                        deferred.reject(e);
                    };
                    reader.readAsDataURL(file);

                    return deferred.promise;
                }

            }); //change

        } //link
    }; //return
});

adminModule.controller("loginController", function ($scope, $window, $http, Base64) {
    Admin.initialize($scope);
    $scope.submitAdminForm = function () {
        $scope.submitted = true;
        if ($scope.loginForm.$invalid == true) return;
        var credentials = {
            username: $scope.username,
            password: $scope.password
        }
        var auth = Base64.encode($scope.username + ':' + $scope.password)
        $http({
            method: 'GET',
            url: Util.serviceURL + 'login',
            headers: {
                'Authorization': 'Basic ' + auth,
            }
        }).success(function (result, status) {
            if (status >= 200 && status < 400) {
                if ($scope.rememberMe) {
                    Admin.checkCookie($scope);
                }
                sessionStorage[Util.username] = $scope.username;
                sessionStorage[Util.auth] = auth;
                $window.location.href = 'MainPanel.html';
            } else if (status >= 400) {
                $(".alert-danger").removeClass("hidden");
                sessionStorage[Util.username] = undefined;
                sessionStorage[Util.auth] = undefined;
            }
        }).error(function (result, status) {
            alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
        });
    }

    $scope.hasError = function (field, validation) {
        if (validation) {
            return ($scope.form[field].$dirty && $scope.form[field].$error[validation]) || ($scope.submitted && $scope.form[field].$error[validation]);
        }
        return ($scope.form[field].$dirty && $scope.form[field].$invalid) || ($scope.submitted && $scope.form[field].$invalid);
    };
});

adminModule.controller("loadController", function ($scope, $window, $http, Base64) {
    var auth = 'Basic ' + sessionStorage[Util.auth];
    $http({
        method: 'GET',
        url: Util.serviceURL + 'admin/profile',
        headers: {
            'Authorization': auth,
        }
    }).success(function (result, status) {
        Admin.users = result.users;
        var markup = "";
        for (var i = 0; i < result.users.length; i++) {
            markup += "<a href='#' data-id='" + result.users[i]._id + "' class='list-group-item'>" + result.users[i].lastname + " " + result.users[i].firstname
             + "<span class='badge " + (result.users[i].banStatus ? "" : "hidden") + "'>Ban</span></a>";
        }
        $(".list-users").html(markup);
        Admin.initUserClick($(".list-users"), $http);
        $(".user-count").text(Admin.users.length);
        Admin.setDashboardTopUsers();
    }).error(function (result, status) {
        alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
    });

    $http({
        method: 'GET',
        url: Util.serviceURL + 'admin/category',
        headers: {
            'Authorization': auth,
        }
    }).success(function (result, status) {
        Admin.reinitializeCategories(result, $http);
        $(".category-count").text(Admin.categories.length);
    }).error(function (result, status) {
        alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
    });
    Admin.initReportList($http);
});

adminModule.controller("postCategoryController", function ($scope, $http, Base64) {
    $scope.submitCategoryForm = function () {
        $scope.submitted = true;
        if ($scope.postCategory.$invalid == true) return;
        $('#overlay').fadeIn();
        var imageName = $("#categoryImageSelector").val().replace("C:\\fakepath\\", "").replace(".jpg", "").replace(".png", "");
        var auth = 'Basic ' + sessionStorage[Util.auth];
        var file = jQuery('#categoryImageSelector')[0].files[0];
        var formData = new FormData();
        var action = $(".panel-category-info").attr("data-action");
        if (imageName) {
            var file = jQuery('#categoryImageSelector')[0].files[0];
            var formData = new FormData();
            formData.append('image', file);
            jQuery.ajax({
                url: Util.serviceURL + 'upload/category/' + imageName,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'PUT',
                success: function (data) {

                },
                error: function () {
                    alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
                }
            });
        }

        if ($(".panel-category-info").attr("data-action") == "CREATE") {
            $http({
                method: 'POST',
                url: Util.serviceURL + 'admin/category',
                headers: {
                    'Authorization': auth
                },
                data: { name: $scope.catName, icon: imageName }
            }).success(function (result, status) {
                $http({
                    method: 'GET',
                    url: Util.serviceURL + 'admin/category',
                    headers: {
                        'Authorization': auth,
                    }
                }).success(function (result, status) {
                    Admin.reinitializeCategories(result, $http);
                    $scope.catName = "";
                    $scope.iconImage = "";
                    $scope.icon = "";
                    $scope.submitted = false;
                    $(".panel-category-info").addClass("hidden");
                    $("#editCategory").attr("disabled", "");
                    $("#removeCategory").attr("disabled", "");
                    $("#contentOfImage").removeAttr("src");
                    $('#overlay').fadeOut();
                }).error(function (result, status) {
                    alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
                });
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        } else if ($(".panel-category-info").attr("data-action") == "EDIT") {
            var uploadData = undefined;
            if (imageName) {
                uploadData = { name: $scope.catName, icon: imageName };
            } else {
                uploadData = { name: $scope.catName };
            }
            $http({
                method: 'POST',
                url: Util.serviceURL + 'admin/category/' + $(".panel-category-info").attr("data-id"),
                headers: {
                    'Authorization': auth
                },
                data: uploadData
            }).success(function (result, status) {
                $http({
                    method: 'GET',
                    url: Util.serviceURL + 'admin/category',
                    headers: {
                        'Authorization': auth,
                    }
                }).success(function (result, status) {
                    Admin.reinitializeCategories(result, $http);
                    $scope.catName = "";
                    $scope.iconImage = "";
                    $scope.icon = "";
                    $scope.submitted = false;
                    $(".panel-category-info").addClass("hidden");
                    $("#editCategory").attr("disabled", "");
                    $("#removeCategory").attr("disabled", "");
                    $("#contentOfImage").removeAttr("src");
                    $('#overlay').fadeOut();
                }).error(function (result, status) {
                    alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
                });
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        }
    }

    $scope.hasError = function (field, validation) {
        if (validation) {
            return ($scope.form[field].$dirty && $scope.form[field].$error[validation]) || ($scope.submitted && $scope.form[field].$error[validation]);
        }
        return ($scope.form[field].$dirty && $scope.form[field].$invalid) || ($scope.submitted && $scope.form[field].$invalid);
    };
});

adminModule.controller("removeCategory", function ($scope, $http, Base64) {
    $scope.removeSelectedCategory = function () {
        var auth = 'Basic ' + sessionStorage[Util.auth];
        $http({
            method: 'DELETE',
            url: Util.serviceURL + 'admin/category/' + $(".list-categories .selected").data("id"),
            headers: {
                'Authorization': auth,
            }
        }).success(function (result, status) {
            $(".list-categories .selected").remove();
            $("#categoryName").val();
            $("#imageValidator").val();
            $("#categoryName").trigger("change");
            $("#imageValidator").trigger("change");
            $scope.submitted = false;
            $(".panel-category-info").addClass("hidden");
            $("#editCategory").attr("disabled", "");
            $("#removeCategory").attr("disabled", "");
            $("#contentOfImage").removeAttr("src");
        }).error(function (result, status) {
            alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
        });
    }
});

adminModule.controller("reportPagingController", function ($scope, $http) {
    $scope.previousPage = function () {
        Admin.initReportList($http, "PREV");
    }
    $scope.nextPage = function () {
        Admin.initReportList($http, "NEXT");
    }
});

adminModule.controller("filterReportController", function ($scope, $http) {
    $(".filters input").change(function () {
        $(".filters input").prop("checked", false);
        $(this).prop("checked", true);
        Admin.initReportList($http);
    });
});

adminModule.controller("notificationController", function ($scope, $timeout) {
    //$scope.tickInterval = 10000;
    //var tick = function () {
    //    alert("Ticker");
    //    $timeout(tick, $scope.tickInterval);
    //}
    //$timeout(tick, $scope.tickInterval);
});

var Admin = {
    initialize: function ($scope) {
        $scope.username = Admin.getCookie("username");
        $scope.password = Admin.getCookie("password");
    },
    firstTimeLoadNotifications: true,
    users: undefined,
    categories: undefined,
    reports: undefined,
    reportPage: 1,
    selectedCategory: undefined,
    getCookie: function (c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
            c_value = null;
        }
        else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    },
    setCookie: function (c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    },
    checkCookie: function ($scope) {
        var username = Admin.getCookie("username");
        username = $scope.username;
        password = $scope.password;
        if (username != null && username != "") {
            Admin.setCookie("username", username, 365);
            Admin.setCookie("password", password, 365);
        }
    },
    initEvents: function () {
        $(".option-item").click(function () {
            $(".section-item").addClass("hidden");
            $("#" + $(this).data("route")).removeClass("hidden");
        });
        $("#categoryImageSelector").change(function () {
            $("#imageValidator").val($(this).val());
            $("#imageValidator").trigger("change");
        });
    },
    initUserClick: function ($section, $http) {
        $section.find(".list-group-item").click(function () {
            $section.find(".list-group-item").removeClass("selected");
            $(this).addClass("selected");
            var userId = $(this).data("id");
            var user = undefined;
            for (var i = 0; i < Admin.users.length; i++) {
                if (userId == Admin.users[i]._id) {
                    user = Admin.users[i];
                    break;
                }
            }
            if (user.banStatus) {
                $("#banStatus").removeClass("hidden");
            } else {
                $("#banStatus").addClass("hidden");
            }
            $(".panel-user-info").data("id", user._id);
            $("#userFirstName").text(user.firstname);
            $("#userLastName").text(user.lastname);
            $("#userEmail").text(user.email);
            $("#userCreated").text(Admin.formatDate(user.createdAt));
            $("#userReported").text(user.reportCount);
            $("#userFixCount").text(user.fixCount);
            document.getElementById("userPhoto").src = user.image;
            $(".panel-user-info").removeClass("hidden");
        });

        $("#userChangeBan").off().on('click', function () {
            var auth = 'Basic ' + sessionStorage[Util.auth];
            var userId = $(".panel-user-info").data("id");
            $http({
                method: 'PUT',
                url: Util.serviceURL + 'admin/profile/ban/' + userId,
                headers: {
                    'Authorization': auth,
                }
            }).success(function (result, status) {
                var user = undefined;
                for (var i = 0; i < Admin.users.length; i++) {
                    if (userId == Admin.users[i]._id) {
                        user = Admin.users[i];
                        break;
                    }
                }
                if (!user.banStatus) {
                    $("#banStatus").removeClass("hidden");
                    $(".list-users .selected .badge").removeClass("hidden");
                } else {
                    $("#banStatus").addClass("hidden");
                    $(".list-users .selected .badge").addClass("hidden");
                }
                user.banStatus = !user.banStatus;
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        });

        $("#userDelete").off().on('click', function () {
            var auth = 'Basic ' + sessionStorage[Util.auth];
            var userId = $(".panel-user-info").data("id");
            $http({
                method: 'DELETE',
                url: Util.serviceURL + 'admin/profile/' + userId,
                headers: {
                    'Authorization': auth,
                }
            }).success(function (result, status) {
                $(".panel-user-info").addClass("hidden");
                var user = undefined;
                for (var i = 0; i < Admin.users.length; i++) {
                    if (userId == Admin.users[i]._id) {
                        Admin.users.splice(i, 1);
                    }
                }
                $(".list-users .list-group-item[data-id=" + userId + "]").remove();
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        });
    },
    initCategoryClick: function ($section, $http) {
        $section.find(".list-group-item").off().on('click', function () {
            $section.find(".list-group-item").removeClass("selected");
            $(this).addClass("selected");
            var categoryId = $(this).data("id");
            for (var i = 0; i < Admin.categories.length; i++) {
                if (categoryId == Admin.categories[i]._id) {
                    Admin.selectedCategory = Admin.categories[i];
                    break;
                }
            }
            $("#contentOfImage").attr("src", $(this).find("img").attr("src"));
            $("#categoryName").val(Admin.selectedCategory.name);
            $(".panel-category-info").data("action", "NONE");
            $("#editCategory").removeAttr("disabled");
            $("#removeCategory").removeAttr("disabled");
        });

        $("#addCategory").off().on('click', function () {
            $("#categoryName").val("");
            $(".panel-category-info").attr("data-action", "CREATE");
            $("#categoryImageSelector").val("");
            $("#contentOfImage").removeAttr("src");
            $(".panel-category-info").removeClass("hidden");
        });

        $("#editCategory").off().on('click', function () {
            $("#categoryName").val(Admin.selectedCategory.name);
            $(".panel-category-info").attr("data-action", "EDIT");
            $(".panel-category-info").attr("data-id", $(".list-categories .selected").data("id"));
            $("#imageValidator").val("nedim");
            $("#imageValidator").trigger("change");
            $("#categoryName").trigger("change");
            $("#categoryImageSelector").val("");
            $("#contentOfImage").attr("src", $(".list-categories .selected img").attr("src"));
            Admin.selectedImage = $("#contentOfImage").attr("src");
            $(".panel-category-info").removeClass("hidden");
        });
    },
    selectedImage: undefined,
    reinitializeCategories: function (result, $http) {
        Admin.categories = result.categories;
        var markup = "";
        for (var i = 0; i < result.categories.length; i++) {
            markup += "<a href='#' data-id='" + result.categories[i]._id + "' class='list-group-item'><img class='icon margin-right-10' src='" + Util.serviceURL + "upload/" + result.categories[i].icon + "'/>" + result.categories[i].name + "</a>";
        }
        $(".list-categories").html(markup);
        Admin.initCategoryClick($(".list-categories"), $http);
    },
    selectedChange: false,
    initReports: function ($http) {
        $(".list-reports .list-group-item").off().on('click', function () {
            $(".list-reports .list-group-item").removeClass("selected");
            $(this).addClass("selected");
            var selectedReport = Admin.getReport($(this).data("id"));
            var mapURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + selectedReport.latitude + "," + selectedReport.longitude
                       + "&zoom=17&size=385x350&maptype=roadmap&markers=color:red%7Clabel:%7C" + selectedReport.latitude + "," + selectedReport.longitude
                       + "&key=AIzaSyBLWnxEROIq3olQc5ukP9QtDCcYP_-2g2I";
            $('#mapImage').attr('src', mapURL);
            var imagePath = "";
            if (selectedReport.image)
                imagePath = Util.serviceURL + selectedReport.image.slice(2).replace(".png", "").replace(".jpg", "").replace("uploads", "upload").replace("fullsize", "thumbs");
            $("#reportPhoto").attr("src", imagePath).change();
            $(".panel-report-info #reportTitle").text(selectedReport.title);
            $(".panel-report-info #reportAddress").text(selectedReport.address);
            $(".panel-report-info #reportCategory").text(selectedReport.category.name);
            var user = Admin.getUser(selectedReport.reportedBy);
            if (user) {
                $(".panel-report-info #reportUser").text(user.lastname + " " + user.firstname);
            }
            $(".panel-report-info #reportDate").text(Admin.formatDate(selectedReport.date));
            $(".panel-report-info #reportViews").text(selectedReport.viewCount);
            $(".panel-report-info #reportLikes").text(selectedReport.likeCount);
            $(".panel-report-info #reportDescription").text(selectedReport.description);
            selectedReport.aprovedStatus ? $("#badgeAproved").removeClass("hidden") : $("#badgeAproved").addClass("hidden");
            selectedReport.resolveStatus ? $("#badgeResolved").removeClass("hidden") : $("#badgeResolved").addClass("hidden");
            Admin.selectedChange = true;
            $("#toggleAproved").prop("checked", selectedReport.aprovedStatus).change();
            $("#toggleResolve").prop("checked", selectedReport.resolveStatus).change();
            Admin.selectedChange = false;
            $(".panel-report-info").removeClass("hidden");
            var imageURL = Util.serviceURL + selectedReport.image.replace(".png", "").replace(".jpg", "").replace("uploads", "upload");
            $("#reportPhoto").off().on('click', function () {
                $('#imagePreview').attr('src', imageURL);
                $('#imagemodal').modal('show');
            });
        });

        $("#toggleAproved").on('change', function () {
            if (Admin.selectedChange) {
                return;
            }
            $('#overlay').fadeIn();
            var state = $(this).is(":checked");
            var reportId = $(".list-reports .list-group-item.selected").data("id");
            var auth = 'Basic ' + sessionStorage[Util.auth];
            $http({
                method: 'PUT',
                url: Util.serviceURL + 'admin/report/approve/' + reportId,
                headers: {
                    'Authorization': auth,
                }
            }).success(function (result, status) {
                if (state) {
                    $(".list-reports .list-group-item.selected .aprove-status").removeClass("color-red").addClass("color-green");
                    $("#badgeAproved").removeClass("hidden");
                } else {
                    $(".list-reports .list-group-item.selected .aprove-status").removeClass("color-green").addClass("color-red");
                    $("#badgeAproved").addClass("hidden");
                }
                var selectedReport;
                for (var i = 0; i < Admin.reports.length; i++) {
                    if (reportId == Admin.reports._id) {
                        Admin.reports[i].aprovedStatus = !Admin.reports[i].aprovedStatus;
                        break;
                    }
                }
                $('#overlay').fadeOut();
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        });

        $("#toggleResolve").on('change', function () {
            if (Admin.selectedChange) {
                return;
            }
            $('#overlay').fadeIn();
            var state = $(this).is(":checked");
            var reportId = $(".list-reports .list-group-item.selected").data("id");
            var auth = 'Basic ' + sessionStorage[Util.auth];
            $http({
                method: 'PUT',
                url: Util.serviceURL + 'admin/report/resolve/' + reportId,
                headers: {
                    'Authorization': auth,
                }
            }).success(function (result, status) {
                if (state) {
                    $(".list-reports .list-group-item.selected .resolve-status").removeClass("color-red").addClass("color-green");
                    $("#badgeResolved").removeClass("hidden");
                } else {
                    $(".list-reports .list-group-item.selected .resolve-status").removeClass("color-green").addClass("color-red");
                    $("#badgeResolved").addClass("hidden");
                }
                var selectedReport;
                for (var i = 0; i < Admin.reports.length; i++) {
                    if (reportId == Admin.reports._id) {
                        Admin.reports[i].resolveStatus = !Admin.reports[i].resolveStatus;
                        break;
                    }
                }
                $('#overlay').fadeOut();
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        });

        $("#reportDelete").on('click', function () {
            $('#overlay').fadeIn();
            var reportId = $(".list-reports .list-group-item.selected").data("id");
            var auth = 'Basic ' + sessionStorage[Util.auth];
            $http({
                method: 'DELETE',
                url: Util.serviceURL + 'admin/report/' + reportId,
                headers: {
                    'Authorization': auth,
                }
            }).success(function (result, status) {
                for (var i = 0; i < Admin.reports.length; i++) {
                    if (reportId == Admin.reports[i]._id) {
                        Admin.reports.splice(i, 1);
                    }
                }
                $(".list-reports .list-group-item.selected").remove();
                $(".panel-report-info").addClass("hidden");
                $('#overlay').fadeOut();
            }).error(function (result, status) {
                alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            });
        });
    },
    getReport: function (_id) {
        for (var i = 0; i < Admin.reports.length; i++) {
            if (Admin.reports[i]._id == _id) {
                return Admin.reports[i];
            }
        }
    },
    getUser: function (_id) {
        for (var i = 0; i < Admin.users.length; i++) {
            if (Admin.users[i]._id == _id) {
                return Admin.users[i];
            }
        }
    },
    initReportList: function ($http, direction) {
        if (direction) {
            if (direction == "NEXT")
                Admin.reportPage++;
            else {
                if (Admin.reportPage == 1) return;
                Admin.reportPage--;
            }
        }
        $(".panel-report-info").addClass("hidden");
        var auth = 'Basic ' + sessionStorage[Util.auth];
        var filters = "";
        if ($(".filters input:checked").length > 0) {
            filters = "?sort=" + $(".filters input:checked").val() + "&crit=-1";
        }
        $('#overlay').fadeIn();
        $http({
            method: 'GET',
            url: Util.serviceURL + 'admin/report/' + Admin.reportPage + filters,
            headers: {
                'Authorization': auth,
            }
        }).success(function (result, status) {
            if (result.reports.length == 0) {
                if (direction) {
                    if (direction == "NEXT") {
                        Admin.reportPage--;
                    } else {
                        Admin.reportPage++;
                    }
                }
                $('#overlay').fadeOut();
                return;
            }
            $(".report-page").text(Admin.reportPage);
            Admin.reports = result.reports;
            var markup = "";
            for (var i = 0; i < result.reports.length; i++) {
                markup += "<a href='#' data-id='" + result.reports[i]._id + "' class='list-group-item'>"
                 + "<span class='report-list-title'>" + result.reports[i].title + "</span><br/><span class='report-desc'>" + result.reports[i].description + "</span></span>"
                 + "<span class='badge resolve-status " + (result.reports[i].resolveStatus ? "color-green" : "color-red") + "'>Riješen</span>"
                 + "<span class='badge aprove-status " + (result.reports[i].aprovedStatus ? "color-green" : "color-red") + "'>Odobren</span></a>";
            }
            $(".list-reports").html(markup);
            Admin.initReports($http);
            $(".report-count").text(Admin.reports.length);
            $('#overlay').fadeOut();
        }).error(function (result, status) {
            alert("Servis je privremeno izvan funkcije. Pokušajte ponovo za par minuta, ukoliko se greška i dalje ponavljala, kontaktirajte nas.")
            $('#overlay').fadeOut();
        });
    },
    setDashboardTopUsers: function () {
        var threeUsers = new Array();
        var max = 0;
        for (var j = 0; j < 3; j++) {
            for (var i = 0; i < Admin.users.length; i++) {
                switch (j) {
                    case 0:
                        if (max < Admin.users[i].reportCount) {
                            max = Admin.users[i].reportCount;
                            threeUsers[j] = Admin.users[i];
                        }
                        break;
                    case 1:
                        if (max < Admin.users[i].reportCount && threeUsers[0]._id != Admin.users[i]._id) {
                            max = Admin.users[i].reportCount;
                            threeUsers[j] = Admin.users[i];
                        }
                        break;
                    case 2:
                        if (max < Admin.users[i].reportCount && threeUsers[0]._id != Admin.users[i]._id && threeUsers[1]._id != Admin.users[i]._id) {
                            max = Admin.users[i].reportCount;
                            threeUsers[j] = Admin.users[i];
                        }
                        break;
                }
            }
            max = 0;
        }
        for (var i = 0; i < threeUsers.length; i++) {
            $(".top-user-" + (i + 1)).find(".image-user").attr("src", threeUsers[i].image).change();
            $(".top-user-" + (i + 1)).find(".name-user").text(threeUsers[i].lastname + " " + threeUsers[i].firstname);
            $(".top-user-" + (i + 1)).find(".date-user").text(Admin.formatDate(threeUsers[i].createdAt));
            $(".top-user-" + (i + 1)).find(".reports-user").text(threeUsers[i].reportCount);
        }
    },
    formatDate: function (date) {
        if (date) {
            return date.substring(0, 10);
        } return "N/A";
    }
}