﻿var Util = {
    serviceURL: "http://52.42.245.91:3000/",
    username: 'usernamePMO',
    auth: 'authPMO',
    generateGuid: function () {
        function _p8(s) {
            var p = (Math.random().toString(16) + "000000000").substr(2, 8);
            return s ? p.substr(0, 4) + p.substr(4, 4) : p;
        }
        return _p8() + _p8(true) + _p8(true) + _p8();
    },
    IDGenerate: function () {
        var text = "";
        var hdntxt = "";
        var captchatext = "";
        var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for (var i = 0; i < 7; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;;
    }
}